= Encoding for Robust Immutable Storage (ERIS)
pukkamustard <pukkamustard@posteo.net>
1.0.0-draft
:toc: left
:xrefstyle: short
:sectnums:
:sectanchors:


[abstract]
This document describes the Encoding for Robust Immutable Storage (ERIS). ERIS is an encoding of arbitrary content into a set of uniformly sized, encrypted and content-addressed blocks as well as a short identifier (a URN). The content can be reassembled from the encrypted blocks only with this identifier. The encoding is defined independent of any storage and transport layer or any specific application. We illustrate how ERIS can be used as a building block for robust and decentralized applications.

[WARNING]
====
This specification is not yet stable or released. We are working towards a stable 1.0.0 release. Join the https://lists.sr.ht/~pukkamustard/eris[mailing list] for updates.
====

== Introduction

Unavailability of content on computer networks is a major cause for reduced reliability of networked services <<Polleres2020>>.

Availability can be increased by caching content on multiple peers. However most content on the Internet is identified by its location. Caching location-addressed content is complicated as the content receives a new location.

An alternative to identifying content by its location is to identify content by its content itself. This is called content-addressing. The hash of some content is computed and used as an unique identifier for the content.

Caching content-addressed content and making it available redundantly is much easier as the content is completely decoupled from any physical location. Integrity of content is automatically ensured with content-addressing (when using a cryptographic hash) as the identifier of the content can be computed to check that the content matches the requested identifier.

However, naive content-addressing has certain drawbacks:

- Large content is stored as a large chunk of data. In order to optimize storage and network operations it is better to split up content into smaller uniformly sized blocks and reassemble blocks when needed.
- Confidentiality: Content is readable by all peers involved in transporting, caching and storing content.

ERIS addresses these issues by splitting content into small uniformly sized and encrypted blocks. These blocks can be reassembled to the original content only with access to a short _read capability_, which can be encoded as an URN.

Encodings similar to ERIS are already widely-used in applications and protocols such as GNUNet (see <<_previous_work>>), BitTorrent <<BEP52>>, Freenet <<Freenet>> and others. However, they all use slightly different encodings that are tied to the respective protocols and applications. ERIS defines an encoding independant of any specific protocol or application and decouples content from transport and storage layers. ERIS may be seen as a modest step towards Information-Centric Networking <<RFC7927>>.

=== Objectives

The objectives of ERIS are:

Availability :: Content encoded with ERIS can be easily replicated and cached.
Integrity :: Integrity of content can be verified efficiently.
Confidentiality :: Encoded content can only be decoded with access to the read capability. Peers without access to the read capability can cache and transport individiual blocks without being able to read the content.
URN reference :: ERIS encoded content can be referrenced with a single URN (the encoded read capability).
Storage efficiency :: ERIS can be used to encode small content (< 1 kibibyte) as well as large content (> many gibibyte) with reasonable storage overhead.
Simplicity :: The encoding should be as simple as possible in order to allow correct implementation on various platforms and in various languages.

ERIS can be used to make content available robustly (against network failure or active censorship). ERIS is not suitable for encoding private, end-to-end encrypted communication. The encoding does not provide security properties required for such applications (e.g. forward secrecy). For end-to-end encryption see protocols such as https://otr.im/[OTR], OMEMO <<OMEMO>> or MLS <<MLS>> .

=== Scope

ERIS describes how arbitrary content (sequence of bytes) can be encoded into a set of uniformly sized blocks and an identifier with which the content can be decoded from the set of blocks.

ERIS does not prescribe how the blocks should be stored or transported over network. The only requirement is that a block can be referenced and accessed (if available) by the hash value of the contents of the block. In section <<_storage_and_transport_layers>> we show how existing technology (including IPFS) can be used to store and transport blocks.

There is also no support for grouping content or mutating content. In section <<_mutability_and_namespaces>> we describe how such functionality can be implemented on top of ERIS.

The lack of certain functionalities is intentional. ERIS is an attempt to find a minimal common basis on which higher functionality can be built. Lacking functionality in ERIS is an acknowledgment that there are many ways of implementing such functionality at a different layer that may be optimized for certain use-cases.

=== Previous work

ERIS is inspired and based on the encoding used in the file-sharing application of https://gnunet.org/[GNUNet] - Encoding for Censorship-Resistant Sharing (ECRS) <<ECRS>>.

ERIS differs from ECRS in following points:

Cryptographic primitives :: ECRS itself does not specify any cryptographic primitives but the GNUNet implementation uses the SHA-512 hash and AES cipher. ERIS uses the Blake2b-256 cryptographic hash <<RFC7693>> and the ChaCha20 stream cipher <<RFC8439>>. This improves performance, storage efficiency (as hash references are smaller) and allows a convergence secret to be used (via Blake2b keyed hashing; see <<_convergence_secret>>).
Block size :: ECRS uses a fixed block size of 32 KiB. This can be very inefficient when encoding many small pieces of content. ERIS allows a block size of 1 KiB or 32 KiB, allowing efficient encoding of small and large content (see <<_block_size>>).
URN :: ECRS does not specify an URN for referring to encoded content (this is specified as part of the GNUNet file-sharing application). ERIS specifies an URN for encoded content regardless of encoding application or storage and transport layer (see <<_urn>>).
Namespaces :: ECRS defines two mechanisms for grouping and discovering encoded content (SBlock and KBlock). ERIS does not specify any such mechanisms (see <<_mutability_and_namespaces>>).

Other related projects include Tahoe-LAFS and Freenet. The reader is referred to the ECRS paper <<ECRS>> for an in-depth explanation and comparison of related projects.

ERIS is being developed in close collaboration with the https://datashards.net/[Datashards] initiative.

=== Terminology

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in RFC 2119 <<RFC2119>>.

We use binary prefixes for multiples of bytes, i.e: 1024 bytes is 1 kibibyte (KiB), 1024 kibibytes is 1 mebibyte (MiB) and 1024 mebibytes is 1 gigibytes (GiB).

== Specification of ERIS

=== Cryptographic Primitives

The cryptographic primitives used by ERIS are a cryptographic hash funciton, a symmetric key cipher and a padding algorithm. The hash function and cipher are readily available in open-source libraries such as https://github.com/jedisct1/libsodium[libsodium] or https://monocypher.org/[Monocypher]. The padding algorithm can be implemented with reasonable effort.

==== Cryptographic Hash Function

Blake2b <<RFC7693>> with output size of 256 bit (32 byte). We use the keying feature and refer to the key used for keying Blake2b as the _hashing key_. The hashing key always has a size of 256 bit (32 byte) (see <<_convergence_secret>>).

Provides the functions `Blake2b-256(INPUT, HASHING-KEY)` for keyed hashing and `Blake2b-256(INPUT)` for unkeyed hashing.

==== Symmetric Key Cipher
ChaCha20 (IETF variant) <<RFC8439>>. Provides `ChaCha20(INPUT, KEY)`,  where `INPUT` is an arbirtarty length byte sequence and `KEY` is the 256 bit encryption key. The output is the encrypted byte sequence.

The 32 bit initial counter as well as the 96 bit nonce are set to 0. We can safely use the zero nonce as we never reuse a key.

Decryption is done with the same function where `INPUT` is the encrypted byte sequence.

==== Padding Algorithm

We use a byte padding scheme to ensure that input content size is a multiple of a block size. Provides following functions:

`PAD(INPUT,BLOCK-SIZE)` :: For `INPUT` of size `n` adds a mandatory byte valued `0x80` (hexadecimal) to `INPUT` followed by `m < BLOCK-SIZE - 1` bytes valued `0x00` such that `n + m + 1` is a multiple of `BLOCK-SIZE`.
`UNPAD(INPUT,BLOCK-SIZE)` :: Starts reading bytes from the end of `INPUT` until a `0x80` is read and then returns bytes of `INPUT` before the `0x80`. Throws an error if a value other than `0x00` is read before reading `0x80` or if no `0x80` is read after reading  `BLOCK-SIZE - 1` bytes from the end.

This is the padding algorithm implemented in https://libsodium.gitbook.io/doc/padding[libsodium]footnote:[This padding algorithm is apparently also specified in ISO/IEC 7816-4. However, the speicifcation is not openly available. So, fuck you ISO.].

=== Block Size

ERIS uses two block sizes: 1KiB (1024 bytes) and 32KiB (32768 bytes). The block size must be specified when encoding content.

Both block sizes can be used to encode content of arbitrary size. The block size of 1KiB is an optimization towards smaller content.

The block size is encoded in the read capability and the decoding process is capable of handling both cases.

Implementations MUST suppport encoding and decoding content with both block sizes (1KiB and 32KiB).

==== Recommendation on Block Size Choice

Applications are RECOMMENDED to use a block size of 1KiB for content smaller than 16KiB and a block size of 32KiB for larger content.

When using block size 32KiB to encode content smaller than 1KiB, the content will be encoded in a 32KiB block. This is a storage overhead of over 3100%. When encoding very many pieces of small content (e.g. short messages or cartographic nodes) this overhead might not be acceptable. On the other hand, using block size 1KiB to encode large content is also not efficient, as the content is split into many small 1KiB blocks and must be reassembled using internal nodes (see <<_collect_reference_key_pairs_in_nodes>>). When encoding larger content it is more efficient to use a block size of 32KiB. Using 16KiB as a breaking point is reasonable for most applications.

Note that the best choice of block size may depend on other factors such as number of round-trips to the storage layer. Content larger than 1KiB encoded with block size 1KiB will always be encoded in multiple levels, requiring multiple calls to a storage layer. For certain applications it might be better to minizmize the number of calls to the storage layer at the cost of higher storage overhead.

In other applications the size of the content to be encoded might not be known when encoding starts and block size must be chosen (see <<_streaming>>). In such cases applications should use appropriate heuristics.

=== Convergence Secret

Using the hash of the content as key is called _convergent encryption_.

Because the hash of the content is deterministically computed from the content, the key will be the same when the same content is encoded twice. This results in de-duplication of content. Convergent encryption suffers from two known attacks: The Confirmation Of A File Attack and The Learn-The-Remaining-Information Attack <<Zooko2008>>. A defense against both attacks is to use a _convergence secret_. This results in different encoding of the same content with different convergence secret.

If no convergence secret is specified a null convergence secret MUST be used (32 bytes of zeroes).

The convergence secret is implemented as the keying feature of the Blake2 cryptographic hash <<RFC7693>>.

[NOTE]
====
A group using a shared convergence secret can benefit from the advantages of convergenct encryption while being safe against the known attacks from peers that do not know the convergence secret.
====

=== Encoding

Inputs to the encoding process are:

`CONTENT` :: An arbitary length byte sequence of content to be encoded.
`CONVERGENCE-SECRET` :: A 256 bit (32 byte) byte sequence (see <<_convergence_secret>>).
`BLOCK-SIZE` :: The block size used for encoding in bytes can be either 1024 (1KiB) or 32768 (32KiB) (see <<_block_size>>).

Content is encoded by first splitting into uniformly sized blocks, encrypting the blocks and computing references to the blocks. If there are multiple references to blocks they are collected in nodes that have the same size as content blocks. The nodes are encrypted and references to the nodes are computed. This process is repeated until there is a single root reference.

References to nodes and blocks of content consist of a reference to an encrypted block and a key to decrypt the block - a _reference-key pair_. The process of encrypting a block and computing a reference-key pair is explained in <<_encrypt_block_and_compute_reference_key_pair>>.

The encoding process constructs a tree of reference-key pairs that reference nodes that hold references to nodes of a lower level or to content.

The number of reference-key pairs collected into a node is called the _arity_ of the tree and depends on the block size. For block size 1KiB the arity of the tree is 16, for block size 32KiB the arity is 512.

An encoding of a content that is split into eight blocks is depicted in <<figure_merkle_tree>>. For illustration purposes the tree is of arity 2 (instead of 16 or 512).

[[figure_merkle_tree]]
.Encoding of content as tree. Solid edges are concatenations of reference-key pairs as described in <<_collect_reference_key_pairs_in_nodes>>. Dotted edges are encryption and computation of reference-key pairs as described in <<_encrypt_block_and_compute_reference_key_pair>>.
image::eris-merkle-tree.svg[Merkle Tree,opts=inline]

The block size, the level of the root reference and the root reference-key pair itself are the necessary pieces of information required to decode content. The tuple consisting of block size, level, root reference and key is called the _read capability_.

The encrypted blocks and the read capability are the outputs of the encoding process.

A pseudo-code implementation of the encoding process is provided in the following. Note that the pseudo-code implementation is naive and given for illustration purposes only. It is RECOMMENDED that imlementations use a streaming encoding process (as described in <<_streaming>>) which allows encoding of content larger than the available memory.

[source,pseudocode]
----
ERIS-Encode(CONTENT, CONVERGENCE-SECRET, BLOCK-SIZE):
    // initialize empty list of blocks to be output
    BLOCKS := []

    // initialize level to 0
    LEVEL := 0

    // split the input content into uniformly sized blocks and encode
    LEVEL-0-BLOCKS, RK-PAIRS := Split-Content(CONTENT, CONVERGENCE-SECRET, BLOCK-SIZE)

    // add blocks from level 0 to blocks to be output
    BLOCKS := BLOCKS ++ LEVEL-0-BLOCKS

    // loop until there is a single root reference
    WHILE Length(RK-PAIRS) > 1:
        LEVEL-BLOCKS, RK-Pairs := Collect-RK-Pairs(RK-PAIRS, CONVERGENCE-SECRET, BLOCK-SIZE)

        // add blocks to blocks to be output and increase the level counter
        BLOCKS := BLOCKS ++ LEVEL-BLOCKS
        LEVEL := LEVEL + 1

    // extract the root reference-key pair
    ROOT-RK-PAIR := RK-PAIRS[0]
    ROOT-REFERENCE, ROOT-KEY := ROOT-RK-PAIR

    // return blocks and read-capability
    RETURN BLOCKS, BLOCK-SIZE, LEVEL, ROOT-REFERENCE, ROOT-KEY
----

The sub-process `Split-Content` and `Collect-RK-Pairs` are explained in the following sections.


==== Splitting Input Content into Blocks

Input content is split into blocks of size at most block size such that only the last content block may be smaller than block size.

The last content block is always padded according to the padding algorithm to block size. If the size of the padded last block is larger than block size it is split into content blocks of block size.

A pseudo code implementation:

[source,pseudocode]
----
Split-Content(CONTENT, CONVERGENCE-SECRET, BLOCK-SIZE):
    // initialize list of blocks and reference-key pairs to output
    BLOCKS := []
    RK-PAIRS := []

    // read blocks of size BLOCK-SIZE from CONTENT
    WHILE CONTENT-BLOCK, LAST? := READ(CONTENT, BLOCK-SIZE):

        IF LAST?:
            // pad block if it is the last
            PADDED := PAD(CONTENT-BLOCK, BLOCK-SIZE)

            IF Length(PADDED) > BLOCK-SIZE:
                PADDED-0, PADDED-1 := SPLIT(PADDED, BLOCK-SIZE)
                ENCRYPTED-BLOCK-0, RK-PAIR-0 := Encrypt-Block(PADDED-0, CONVERGENCE-SECRET)
                ENCRYPTED-BLOCK-1, RK-PAIR-1 := Encrypt-Block(PADDED-1, CONVERGENCE-SECRET)
                BLOCKS := BLOCKS ++ [ENCRYPTED-BLOCK-0, ENCRYPTED-BLOCK-1]
                RK-PAIRS := RK-PAIRS ++ [RK-PAIR-0, RK-PAIR-1]
            ELSE:
                ENCRYPTED-BLOCK, RK-PAIR := Encrypt-Block(PADDED, CONVERGENCE-SECRET)
                BLOCKS := BLOCKS ++ [ENCRYPTED-BLOCK]
                RK-PAIRS := RK-PAIRS ++ [RK-PAIR]

         ELSE:
            ENCRYPTED-BLOCK, RK-PAIR := Encrypt-Block(CONTENT-BLOCK, CONVERGENCE-SECRET)
            BLOCKS := BLOCKS ++ [ENCRYPTED-BLOCK]
            RK-PAIRS := RK-PAIRS ++ [RK-PAIR]

    RETURN BLOCKS, RK-PAIRS
----

NOTE: If the length of the last content block is exactly block size, then padding will result in a padded block that is double the block size and must be split.

==== Encrypt Block and Compute Reference-Key Pair

A _reference-key pair_ is a pair consisting of a reference to an encrypted block and the key to decrypt the block. Reference and key are both 32 bytes long. The concatenation of  a reference-key pair is 64 bytes long (512 bits).

The `Encrypt-Block` function encrypts a block and returns the encrypted block along with the reference-key pair:

[source,pseudocode]
----
Encrypt-Block(INPUT, CONVERGENCE-SECRET):
    KEY := Blake2b-256(INPUT, CONVERGENCE-SECRET)
    ENCRYPTED-BLOCK := ChaCha20(INPUT, KEY)
    REFERENCE := Blake2b-256(ENCRYPTED-BLOCK)
    RETURN ENCRYPTED-BLOCK, REFERENCE, KEY
----

The convergence-secret MUST NOT be used to compute the reference to the encrypted block.

==== Collect Reference-Key Pairs in Nodes

Reference-key pairs are collected into nodes of size block size by concatenating reference-key pair. The node is encrypted, and a reference-key pair to the node is computed. This results in a sequence of reference-key pairs that refer to nodes containing reference-key pairs at a lower level - a tree.

If there are less than arity number of references-key pairs to collect in a node, then the node is filled with missing number of _null reference-key pairs_ - 64 bytes of zeros. The size of a node is always equal the block size (implemented with the `FILL-WITH-NULL-RK-PAIRS` function).

A pseudo-code implementation of `Collect-RK-Pairs`:

[source,pseudocode]
----
Collect-RK-Pairs(INPUT-RK-PAIRS, CONVERGENCE-SECRET, BLOCK-SIZE):
    // number of reference-key pairs in a node
    ARITY := BLOCK-SIZE / 64

    // initialize blocks and reference-key pairs to output
    BLOCKS := []
    OUTPUT-RK-PAIRS := []

    // take ARITY reference-key pairs from INPUT-RK-PAIRS at a time
    WHILE RK-PAIRS-FOR-NODE := TAKE(INPUT-RK-PAIRS, ARITY):
        // make sure there are exactly ARITY reference-key pairs in node
        RK-PAIRS-FOR-NODE := FILL-WITH-NULL-RK-PAIRS(RK-PAIRS-FOR-NODE, ARITY)

        // concat reference-key pairs to node
        NODE := CONCAT(RK-PAIRS-FOR-NODE)

        // encrypt node and compute reference-key pair
        BLOCK, RK-TO-NODE := Encrypt-Block(NODE, CONVERGENCE-SECRET)

        // add node to output
        BLOCKS := BLOCKS ++ [BLOCK]
        OUTPUT-RK-PAIRS := OUTPUT-RK-PAIRS ++ [RK-TO-NODE]

    RETURN BLOCKS, OUTPUT-RK-PAIRS
----

==== Streaming

The encoding process can be implemented to encode a stream of content while immediately outputting encrypted blocks when ready and eagerly collecting reference-key pairs to nodes. This allows the encoding of larger-than-memory content.

For an example, see https://inqlab.net/git/guile-eris.git/tree/eris/encode.scm[the reference Guile implementation].

=== Decoding

Given an ERIS read capability and access to blocks via a block-storage the content can be decoded.

[source, pseudocode]
----
ERIS-Decode-Recurse(LEVEL, REFERENCE, KEY):
    IF LEVEL == 0:
        ENCRYPTED-CONTENT-BLOCK := Block-Storage-Get(REFERENCE)
        RETURN ChaCha20(CONTENT-BLOCK, KEY)
    ELSE:
        ENCRYPTED-NODE := Block-Storage-Get(REFERENCE)
        NODE := ChaCha20(ENCRYPTED, KEY)
        OUTPUT := []
        WHILE SUB-REFERENCE, SUB-KEY := Read-RK-Pair-From-Node(NODE):
            OUTPUT := OUTPUT ++ [ERIS-DECODE-Recurse(LEVEL - 1, SUB-REFERENCE, SUB-KEY)]
        RETURN CONCAT(OUTPUT)

ERIS-Decode(BLOCK-SIZE, LEVEL, ROOT-REFERENCE, ROOT-KEY):
    PADDED := ERIS-Decode-Recurse(LEVEL, ROOT-REFERENCE, ROOT-KEY)
    RETURN UNPAD(PADDED, BLOCK-SIZE)
----

Where the block-storage can be accessed as follows:

`Block-Storage-Get(REFERENCE)` :: Returns a block such that `Blake2b-256(Block-Storage-Get(REFERENCE)) == REFERENCE` or throws an error.

A streaming decoding procedure can be implemented where the content can be output block wise and does not need to be kept in memory for unpadding. For an example, see https://inqlab.net/git/guile-eris.git/tree/eris/decode.scm[the reference Guile implementation].

==== Random Access

A decoder that allows random access to the encoded content can be implemented by decoding selected sub-trees.

=== Binary Encoding of Read Capability

The read-capability consisting of the block-size, level of root reference-key pair as well as the root reference-key pair form the necessary pieces of information required to decode content.

We specify an binary encoding of the read-capability 66 bytes:

|===
|Byte offset | Content | Length (in bytes)

| 0 | block size (`0x0a` for block size 1KiB and `0x0f` for block size 32KiB)| 1
| 1 | level of root reference-key pair as unsigned integer | 1
| 2 | root reference | 32
| 34 | root key | 32
|===

The initial field (block size) also encodes the ERIS version. Future versions of ERIS MUST use different codes to encode block sizes.

Note that using a single byte to encode the level limits the size of content that can be encoded with ERIS. However, the size of the largest encodable content is approximately 1e300 TiB, which seems to be sufficient for any conceivable practical applications (including an index of all atoms in the universe).

==== CBOR Tag

The CBOR tag `276` is assigned for a ERIS binary read capability (see <<_cbor_tags_registry>>). This allows efficient references to ERIS encoded content from CBOR.

==== GNU Name System

The GNU Name System <<LSD0001>> is a decentralized and censorship-resistant name system that can be used to resolve memorable names to secure identifiers. A GNU Name System record type for ERIS read capabilities is defined (see <<_gnu_name_system_record_types_registry>>) allowing any ERIS encoded content to be associated with memorable names.

=== URN

A read-capability can be encoded as an URN: `urn:erisx2:BASE32-READ-CAPABILITY`, where `BASE32-READ-CAPABILITY` is the unpadded Base32 <<RFC4648>> encoding of the read capability.

For example the ERIS URN of the UTF-8 encoded string "Hello world!" (with block size 1KiB and null convergence secret):

`urn:erisx2:BIAD77QDJMFAKZYH2DXBUZYAP3MXZ3DJZVFYQ5DFWC6T65WSFCU5S2IT4YZGJ7AC4SYQMP2DM2ANS2ZTCP3DJJIRV733CRAAHOSWIYZM3M`

[NOTE]
====
The URN namespace `erisx2` is used for this experimental version of the encoding. Once finalized the namespace `eris` will be used (e.g. `urn:eris:BIAD77QDJMFAKZYH2DXBUZYAP3MXZ3DJZVFYQ5DFWC6T65WSFCU5S2IT4YZGJ7AC4SYQMP2DM2ANS2ZTCP3DJJIRV733CRAAHOSWIYZM3M`)
====
== Applications

Traditionally encoding schemes similar to ERIS are used for peer-to-peer filesharing. We hope to motivate usage for a much wider scope of applications.

As part of the https://openengiadina.net[openEngiadina] project we are using ERIS to encode small bits of information that constitute "local knowledge" (e.g. geogrpahic information, social and cultural events, etc.) along with the social interactions that created and curated this information (using the  ActivityStreams vocabulary <<ActivityStreams>>). ERIS allows such information to be securely cached on multiple peers to increase the robustness of the system.

ERIS encoded content can be used from existing web technology and RDF as the content can be referenced by an URN. At the same time more decentralized networks can be used (this will be further research as part of the https://dream.public.cat/[DREAM] project).

Other possible applications include package managers such as https://guix.gnu.org/[Guix] to increase availability of software sources and built packages or decentralized and offline-first mapping applications.

=== Storage and Transport Layers

ERIS is defined indepenedant of any storage and transport layer for blocks. The only requireiment is that blocks can be accessed by their reference - the hash of the block content.

Possible storage layers include:

- in-memory hash-map
- key-value store
- files on a file system

Transport mechanisms include:

- HTTP: A simple HTTP endpoint can be used to dereference blocks
- Sneakernet: Blocks can be transported on a physical medium such as a USB stick

More interesting transport and storage layers use the fact that blocks are content-addressed. For example the peer-to-peer network https://ipfs.io/[IPFS] can be used to store and transport blocks (see the https://inqlab.net/git/guile-eris.git/tree/examples/ipfs.org[example] using the reference Guile implementation). The major advantages over using IPFS directly include:

- Content is encrypted and not readable to IPFS peers without the read capability.
- Identifier of blocks and encoded content is not tied to the IPFS network. Applications can transparently use IPFS or any other storage/transport mechanism.

It also seems possible to use the https://named-data.net/[Named Data Networking] infrastructure and forwarding daemons (initial support for using Blake2b as hash function is present in the https://github.com/named-data/ndn-cxx[ndn-cxx library]).

=== Authenticity of Content

The presented encoding ensures integrity of content. Content can not be tampered with wihtout changing the identifier (read capability) of the content. To prove authenticity of encoded content it is sufficient to cryptographically sign the read capability.

We have presented a concrete proposal on how this might be done using a RDF vocabulary and the Ed25519 cryptographic signature scheme <<RDF-Signify>>.

=== Mutability and Namespaces

Encoded content is immutable in the sense that changing the encoded content results in a new identifier. Existing references to the old content need to be updated. This is a property that makes caching efficient and allows ERIS to be used for robust systems.

Nevertheless, there are applications where one wants to reference mutable content. Examples include user profiles or dynamic collections of content. Making small changes to a user profile or adding a piece of content to a collection should preserve the identifiers.

There are many ways of implementing such mutability or "namespaces". ERIS does not specify any particular mechanism. Possible mechanisms include:

- Centralized servers that returns a mutable list of reference to (immutable) content. This is how most HTTP services work.
- Append-only logs where changes are securely appended with cryptographic signatures. The state is computed from the log of changes. This is how peer-to-peer systems such as https://hypercore-protocol.org/[hypercore] or https://scuttlebutt.nz/[Secure ScuttleButt] work.
- Petname system: A system where a dynamic local name can be mapped to a reference. Sophisticated systems that allow delegation of naming authority include https://gnunet.org/en/gns.html[the GNU Name System].
- Commutative Replicated Data Types (CRDTs) are distributed datastructures similar to append-only logs with the advantage that the state of a mutable container can diverge and converge to consistent state eventually. Such structures seem especially suitable when control over a mutable container is shared by multiple parties. We have made a concrete proposal for such mutable containers <<DMC>>.

We believe that the best suited mechanism for handling mutability depends on concrete applications and use-cases. A key value of ERIS is that it is agnostic of such mechanisms and can be used from any of them.

== Test Vectors

=== Machine Readable

A set of test vectors are provided in the  https://inqlab.net/git/guile-eris.git/tree/test-vectors[ERIS repository]. Implementations of the ERIS encoding MUST be able to satisfy the test vectors.

The test vectors are given as machine-readable JSON files. For example the test vector `eris-test-vector-00.json`:

[source,json]
----
{
  "id": 0,
  "spec-version": "1.0.0-draft",
  "name": "short string (block size 1KiB)",
  "description": "Encode the UTF-8 encoding of the string \"Hello world!\" with block-size 1KiB and null convergence-secret.",
  "content": "JBSWY3DPEB3W64TMMQQQ",
  "convergence-secret": "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
  "block-size": 1024,
  "read-capability": {
    "block-size": 1024,
    "level": 0,
    "root-reference": "H77AGSYKAVTQPUHODJTQA7WZPTWGTTKLRB2GLMF5H53NEKFJ3FUQ",
    "root-key": "CPTDEZH4ALSLCBR7INTIBWLLGMJ7MNFFCGX7PMKEAA52KZDDFTNQ"
  },
  "urn": "urn:erisx2:BIAD77QDJMFAKZYH2DXBUZYAP3MXZ3DJZVFYQ5DFWC6T65WSFCU5S2IT4YZGJ7AC4SYQMP2DM2ANS2ZTCP3DJJIRV733CRAAHOSWIYZM3M",
  "blocks": {
    "H77AGSYKAVTQPUHODJTQA7WZPTWGTTKLRB2GLMF5H53NEKFJ3FUQ": "EWZKXK73236ETFGMMFORFLMNIPE5V3S3WDVFECUPI47RFJBA5ZMBHH6HMOZCNFQKOTADCPJMPHTZJNEW4VOKHBSNABYVIZWQDV5GQPECUBDAULOPR2S7ITYQSGGVPPEWJVEZNIUKUFR4XE7GQPUDY3FPFSCUYIISZX6PWLLPNPI5V3RKWQGN2L6LLE5G7TZ5FVPAYUHOES4LGHRKSXYCNQF6IR5HLKX2C2EPVKSU2T6XOSAF5VHUZ2GTQS7BLT3VYP5BYI2WR4GJEYDWLY26TK6ZQ2DYZZBIYSVUIY557FE6QOV3L5X5HCAQEWPYCUKUADOOSMNU7EEONPRMBJU4XLQ66AOOVRQ66OJLHANVLNFDXXPLH6KDVCJBVQWWWI7PA6OGKGPU7ZZPZT2DIBOAUGWM6DVZWWX3DA3GHWS3VY6RQMLAKDXHZRQ6VDVMLMFSULJYHACC7G57CZ2SG7XB24XT3SLJG56PO3Z7YJJYEVP44F44YCZ5YS4NRZKWS4OTFMXGNF25G3GIGSV5NEVVTSO6J5EKEXWTX74X27HYI4UZ45YF675423AWYUVTPVLUWOJMGANQRDWYOPFE5QH6JUINCH5NYZUHYPZP6WHC4IVOLYFDAUNOWLRVR37BLT5E44VVJ6XDQZAS2XT6G2XM3RJUUQEYD2RRFBWGPNSOJ2RUPE654GKHRDCKUX2MZ6D43LKI2DKCF7QEPYWJWJH6EI74NQNOLCHEAUFEXH5ZXUXO6JJ5PKOXGL4RBOGCP2X2RYXOJOCT55BAGCRQHID2TRO7NPZWGQNMSSWHOAXY6JFCVFXXGR4JM62HHXZTKODD7NYXO7EUS3GMY2NDQFENM3XKNAI5MFNLL7ERMPSIXHAJ44ASIDZS7RPZ542SLH7XONZ6PMCPI4V66ALJJTTXMJAEU35YPH3UD7UHBCM4OI3SDGTUL3TQQWMDIFBNECJN7FNAWRXTWCXM6CIILVYAITWSEDIDEMLBKR5KIGE5SQTW2ITIIA725SNZO3PJMQCAPJI4H3QXVPKG4OZIOTENU2VW3W3PNAYVE65YJBQGPY6M6LRQYGPYYSEFTRPW3YXGGC2ICFROUD7FXCFXVD6OWA4B6LDFDX4LPF4H7525BVRBNW2ZLMXZUXCFZSZOSSP7VKBCWIDJ72XSR43YFKTL5TADVXDF3RN2HHAGKXWOXINMJJLRE4K72H54IOROFS4FD5QYXWSJWH4ENYC4PAOJ6JELRFYC6RMXP73VR745WY4ZOFQTRQ5ZEA2C3M7JTQUVKV26XGVVHBYA7NEMRPZNVRXHCKYN3CGJSICBUFGMHSSDBTRIF3BCPVMLRBU25DFGGM4LEEL4KTIAJITYY5XPR4XDRD55PEDVOUL342IXCNEBTTPPLMPV6EJYUFJS42R4XLDOT7NOFPLTZUBLWSLL7IVZNPNI6DZ4CR7YEQP72DDUWDJJTKACT35JLFPDW3M2VUOJF3CUWN6FYN5YJJSXYMXSVDZDVIAJYF2HOPQEHLMRF3MJAXMTLMCOIARLFZKAGRSW6PWQZ7ZJLCQAPSJTPNDA2SLUA3UHH34NWEPTAVWOBDPNTMT27TK5P4VKLE2YEJHKWE6SJA3V7A3UPQS24SWDJ2BPOV7JG23ZVIA"
  }
}
----

The fields of JSON test vectors are:

`id` :: Numeric identifier of the test vector.
`spec-version` :: Version of ERIS specification
`name` :: Short human readable name.
`description` :: Human readable description of the test.
`content` :: The binary content to be encoded as Base32 (unpadded) string.
`convergence-secret` :: The convergence secret to be used as Base32 string.
`block-size` :: Block size that should be used for encoding in bytes (either 1024 or 32768).
`read-capability` :: JSON map containing the components of the read capability. This is not used in tests but is here as a help for developers.
`urn` :: The ERIS URN of the content.
`blocks` :: A JSON map of blocks required to decode the content given the URN. Key and field are encoded as Base32 strings.

Implementations MUST verify that the content encodes to the URN given the specified block size and convergence secret and verify that given the URN and blocks the content can be decoded.

=== Large content

In order to verify implementations that encode content by streaming (see <<_streaming>>) URNs of large contents that are generated in a specified way are provided:

|===
|Test name | Content size | Block size | URN | Level of root reference
| 100MiB (block size 1KiB) | 100MiB |  1KiB | `urn:erisx2:BICXPZNDNXFLO4IOMF6VIV2ZETGUJEUU7GN4AHPWNKEN6KJMCNP6YNUMVW2SCGZUJ4L3FHIXVECRZQ3QSBOTYPGXHN2WRBMB27NXDTAP24` | 5
| 1GiB (block size 32KiB) | 1GiB | 32KiB | `urn:erisx2:B4BFG37LU5BM5N3LXNPNMGAOQPZ5QTJAV22XEMX3EMSAMTP7EWOSD2I7AGEEQCTEKDQX7WCKGM6KQ5ALY5XJC4LMOYQPB2ZAFTBNDB6FAA` | 2
| 256GiB (block size 32KiB) | 256GiB | 32KiB | `urn:erisx2:B4BZHI55XJYINGLXWKJKZHBIXN6RSNDU233CY3ELFSTQNSVITBSVXGVGBKBCS4P4M5VSAUOZSMVAEC2VDFQTI5SEYVX4DN53FTJENWX4KU` | 3
|===

Content is the ChaCha20 stream using a null nonce and the key which is the Blake2b hash of the UTF-8 encoded test name (e.g. `KEY := Blake2b-256("100MiB (block size 1KiB)")`). The ChaCha20 stream can be computed by encoding a null byte sequence (e.g. `CHACHA20_STREAM := ChaCha20(NULL, KEY)`).

== Implementations

A list of known implementations that satisify the test vectors:

|===
| Name | Programming language | License | Notes | Homepage

| `guile-eris` | Guile | GPL-3.0-or-later | Reference implementation | https://inqlab.net/git/eris.git/
| `elixir-eris` | Elixir | GPL-3.0-or-later | Used in the https://gitlab.com/openengiadina/cpub[CPub] ActivityPub server | https://inqlab.net/git/elixir-eris.git/
| `eris` | Go |  BSD-3-Clause | | https://github.com/cjslep/eris
| `eris` | Nim | ISC | | https://git.sr.ht/~ehmry/eris
| `js-eris` | JavaScript | LGPL-3.0-or-later | | https://inqlab.net/git/js-eris.git/
|===

Further implementations are under development in https://hg.sr.ht/~arnebab/wisperis/[Wisp], https://gitlab.com/emacsen/pyeris[Python] and https://gitlab.com/public.dream/dromedar/ocaml-eris[OCaml].

== Mailing List

A mailing list for general discussion on ERIS is available at https://lists.sr.ht/~pukkamustard/eris[&#126;pukkamustard/eris@lists.sr.ht].

Please feel free to direct any questions or comments regarding the specification to the mailing list. You are also invited to share your implementations and use-cases.

== IANA Considerations

=== CBOR Tags Registry

This specification requires the assignment of a CBOR tag for a binary ERIS read capability.  The tag is added to the CBOR Tags Registry as defined in RFC 8949 <<RFC8949>>.

|===
| Tag | Data Item   | Semantics
| 276 | byte string | ERIS binary read capability (see <<_binary_encoding_of_read_capability>>)
|===

== GANA Considerations

===  GNU Name System record types registry

GANA <<GANA>> is requested to add an entry into the "GNU Name System record types" registry as follows:

|===
| Number | Name | Comment | References
| 65557  | ERIS_READ_CAPABILITY | Encoding for Robust Immutable Storage (ERIS) binary read capability | http://purl.org/eris
|===

== Acknowledgments

Thanks to Cory Slep, Arne Babenhauserheide, Serge Wroclawski, Christopher Lemmer Webber, Christian Grothoff, Natacha, Hellekin, Nemael, TG, Devan, Emery, Arie, Allen and many others for the discussions, suggestions and support.

Development of ERIS has been supported by the https://nlnet.nl[NLNet Foundation] trough the https://nlnet.nl/discovery/[NGI0 Discovery Fund].

:sectnums!:
== Changelog

The most recent version is published at http://purl.org/eris.

[discrete]
=== link:http://purl.org/eris[v1.0.0-draft (UNRELEASED)]

[discrete]
==== Added

- CBOR Tag for ERIS binary read capability
- GANA GNU Name System record types entry

[discrete]
==== Changed

- Encoding of block size in binary read capability: Use 0x0a for block size 1KiB (instead of 0x00) and 0x0f for block size 32KiB (instead of 0x01)

[discrete]
=== link:eris-v0.2.0.html[v0.2.0 (7. December 2020)]

Major update of encoding that removes the _verification capability_ - ability to verify integrity of content without reading content.

[discrete]
=== link:eris-v0.1.html[v0.1.0 (11. June 2020)]

Initial version.


== Copyright

This work is licensed under a http://creativecommons.org/licenses/by-sa/4.0/[Creative Commons Attribution-ShareAlike 4.0 International License].

== References

[bibliography]
=== Normative References

- [[[GANA]]] GNUnet e.V., https://gana.gnunet.org/[GNUnet Assigned Numbers Authority (GANA)], 2020.
- [[[LSD0001]]] M. Schanzenbach, C. Grothoff, B. Fix, https://lsd.gnunet.org/lsd0001[The GNU Name System], 2021.
- [[[RFC2119]]] S. Bradner, https://tools.ietf.org/html/rfc2119[Key words for use in RFCs to Indicate Requirement Levels], 1997.
- [[[RFC4648]]] S. Josefsson, https://tools.ietf.org/html/rfc4648[The Base16, Base32, and Base64 Data Encodings], 2006.
- [[[RFC8949]]] C. Bormann & P. Hoffman. https://tools.ietf.org/html/rfc8949[Concise Binary Object Representation (CBOR)], 2020.
- [[[RFC7693]]] M-J. Saarinen and J-P. Aumasson, https://tools.ietf.org/html/rfc7693[The BLAKE2 Cryptographic Hash and Message Authentication Code (MAC)], 2015.
- [[[RFC8439]]] Nir and Langley, https://tools.ietf.org/html/rfc8439[ChaCha20 and Poly1305 for IETF Protocols], 2018.
- [[[RFC8141]]] Saint-Andre, Filament and Klensin, https://tools.ietf.org/html/rfc8141[Uniform Resource Names (URNs)], 2017.

[bibliography]
=== Informative References
- [[[ActivityStreams]]] Snell and Prodromou, https://www.w3.org/TR/activitystreams-core/[Activity Streams 2.0], 2017.
- [[[BEP52]]]  http://bittorrent.org/beps/bep_0052.html[The BitTorrent Protocol Specification v2], 2017.
- [[[DMC]]] pukkamustard, http://purl.org/dmc/spec[Distributed Mutable Containers], 2020.
- [[[ECRS]]] Grothoff, et al., https://grothoff.org/christian/ecrs.pdf[An encoding for censorship-resistant sharing], 2003.
- [[[Freenet]]] Clarke, et al., http://bourbon.usc.edu/cs694-s09/papers/freenet.pdf[Freenet: A distributed anonymous information storage and retrieval system], 2001.
- [[[MLS]]] Omara, et al., https://messaginglayersecurity.rocks/mls-architecture/draft-ietf-mls-architecture.html[The Messaging Layer Security (MLS) Architecture (DRAFT)], 2020.
- [[[OMEMO]]] Straub, et al., https://xmpp.org/extensions/xep-0384.html[XEP-0384: OMEMO Encryption], 2020.
- [[[Polleres2020]]] Polleres, et al., https://epub.wu.ac.at/6371/1/IPM_workingpaper_02_2018.pdf[A more decentralized vision for Linked Data], 2020.
- [[[RDF-Signify]]] pukkamustard, http://purl.org/signify/spec[RDF Signify], 2020.
- [[[RFC7927]]] Kutscher et. al. https://tools.ietf.org/html/rfc7927[Information-Centric Networking (ICN) Research Challenges], 2016.
- [[[Zooko2008]]] Zooko Wilcox-O'Hearn. https://tahoe-lafs.org/hacktahoelafs/drew_perttula.html[Drew Perttula and Attacks on Convergent Encryption], 2008.

