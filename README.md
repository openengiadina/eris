# Encoding for Robust Immutable Storage (ERIS)

ERIS is an encoding for arbitrary content into uniformly sized encrypted blocks that can be reassembled only in possession of a short read capability that can be encoded as an URN.

This repository contains the specification documents.

The latest version of the specification is published at: [http://purl.org/eris](http://purl.org/eris). Please use this URL as a stable reference to the specification.

## Status

ERIS is considered to be experimental. A stable version (1.0.0) is planned to be released in 2021. Comments and feedback are very welcome.

See also the section "Changelog" in the specification document.

## Implementations

A list of known implementations of ERIS:

- [guile-eris](http://inqlab.net/git/guile-eris.git): The reference implementation in Guile Scheme.
- [elixir-eris](http://gitlab.com/openengiadina/elixir-eris): Elixir implementation.
- [eris](https://github.com/cjslep/eris): Go implementation.
- [eris](https://git.sr.ht/~ehmry/eris): Nim implementation.
- [js-eris](https://inqlab.net/git/js-eris.git): Javascript implementation.

See also the section "Implementations" of the specification document.

## Acknowledgments

ERIS was initially developed as part of the [openEngiadina](https://openengiadina.net) project and has been supported by the [NLNet Foundation](https:/nlnet) trough the [NGI0 Discovery Fund](https://nlnet.nl/discovery/).

## Contact

Questions and comments may be directed to the [~pukkamustard/eris@sr.ht](https://lists.sr.ht/~pukkamustard/eris) mailing list or directly to the author (pukkamustard@posteo.net).

You are also invited to share your implementations and use-cases on the mailing list.

## License

- Specification: [CC-BY-SA-4.0](./LICENSES/CC-BY-SA-4.0.txt)
- Test vectors: [CC0-1.0](./LICENSES/CC0-1.0.txt)
