.PHONY: spec
spec: public/index.html

.PHONY: publish
publish: spec
	rsync -rav -e ssh public/ pukkamustard@qfwfq.inqlab.net:/srv/http/inqlab.net-projects/eris/ --delete

public/index.html: spec/eris.adoc spec/eris-merkle-tree.svg
	mkdir -p public
	asciidoctor -o $@ -a webfonts! $<

spec/eris-merkle-tree.svg: spec/eris-merkle-tree.dot
	dot -Tsvg -o $@ $<
