; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(use-modules
  (guix packages)
  ((guix licenses) #:prefix license:)
  (guix download)
  (guix git-download)
  (guix build-system gnu)
  (gnu packages)
  (gnu packages license)
  (gnu packages graphviz)
  (gnu packages guile-xyz)
  (gnu packages ruby))

(package
  (name "eris")
  (version "0.1")
  (source #f)
  (build-system gnu-build-system)
  (arguments '())
  (native-inputs
   `(("asciidoctor" ,ruby-asciidoctor)
     ("graphviz" ,graphviz)
     ("guile-eris" ,guile-eris)))
  (synopsis "Specification of Encoding for Robust Immutable Storage (ERIS)")
  (description #f)
  (home-page "https://inqlab.net/git/eris.git")
  (license license:gpl3+))
